#include "stdafx.h"
#include "highgui\highgui.hpp"
#include "core\core.hpp"
#include "iostream"
#include "string"
#include "cmath"
#include "imgproc\imgproc.hpp"
#include "fstream";

using namespace cv;
using namespace std;

string naming;
vector<string> carplatechar;
vector<int> carplatesloc;

Mat makemegray(Mat rgb) {
	Mat gray = Mat::zeros(rgb.size(), CV_8UC1);
	int height = rgb.rows;
	int width = rgb.cols;

	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width * 3; j += 3) {
			gray.at<uchar>(i, j / 3) = (rgb.at<uchar>(i, j) + rgb.at<uchar>(i, j + 1) + rgb.at<uchar>(i, j + 2)) / 3;
		}
	}
	return gray;
}

Mat binarize(Mat rgb, int threshold) {
	Mat bw = Mat::zeros(rgb.size(), CV_8UC1);
	int height = rgb.rows;
	int width = rgb.cols;

	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			if (rgb.at<uchar>(i, j) > threshold) {
				bw.at<uchar>(i, j) = 255;
			}
		}
	}
	return bw;
}

Mat equalize(Mat bw) {
	int count[256] = { 0 };
	int total = bw.rows * bw.cols;

	for (int i = 0; i < bw.rows; i++) {
		for (int j = 0; j < bw.cols; j++) {
			count[bw.at<uchar>(i, j)]++;
		}
	}

	//Then get prob
	double prob[256] = { 0 };
	for (int i = 0; i < 256; i++) {
		prob[i] = (double)count[i] / (double)total;
	}

	double accprob[256] = { 0 };
	accprob[0] = prob[0];
	for (int i = 1; i < 256; i++) {
		accprob[i] = (prob[i] + accprob[i - 1]);
	}

	//Accumulate
	int newValue[256] = { 0 };
	for (int i = 0; i < 256; i++) {
		newValue[i] = 255 * accprob[i];
	}

	Mat eqimg = Mat::zeros(bw.size(), CV_8UC1);
	for (int i = 0; i < bw.rows; i++) {
		for (int j = 0; j < bw.cols; j++) {
			eqimg.at<uchar>(i, j) = newValue[bw.at<uchar>(i, j)];
		}
	}

	return eqimg;
}

Mat blur(Mat bw, int windowsize) {
	Mat blur = Mat::zeros(bw.size(), CV_8UC1);
	int middle = 0 - ((windowsize - 1) / 2);
	for (int i = windowsize; i < bw.rows - windowsize; i++) {
		for (int j = windowsize; j < bw.cols - windowsize; j++) {
			int avg = bw.at<uchar>(i, j);
			int count = 1;
			for (int a = middle; a <= windowsize; a++) {
				for (int b = middle; b <= windowsize; b++) {
					avg += bw.at<uchar>(i + a, j + b);
					count++;
				}
			}
			blur.at<uchar>(i, j) = avg / count;
		}
	}
	return blur;
}

Mat edgeDetection(Mat img, int threshold) {
	Mat edge = Mat::zeros(img.size(), CV_8UC1);
	for (int i = 1; i < img.rows-1; i++) {
		for (int j = 1; j < img.cols-1; j++) {
			int x1 = (img.at<uchar>(i-1, j - 1) + img.at<uchar>(i, j-1) + img.at<uchar>(i+1, j -1)) /3;
			int x2 = (img.at<uchar>(i-1, j + 1) + img.at<uchar>(i, j+1) + img.at<uchar>(i+1, j + 1))/3;
			if (abs(x1 - x2) > threshold) {
				edge.at<uchar>(i, j) = 255;
			};
		}
	}
	return edge;
}

Mat dilation(Mat img, int threshold) {
	Mat dilated = Mat::zeros(img.size(), CV_8UC1);

	for (int i = threshold; i < img.rows - threshold; i++) {
		for (int j = threshold; j < img.cols - threshold; j++) {
			for (int a = -threshold; a <= threshold; a++) {
				for (int b = -threshold; b <= threshold; b++) {
					if (img.at<uchar>(i + a, j + b) == 255) {
						dilated.at<uchar>(i, j) = 255;
					}
				}
			}
		}
	}
	return dilated;
}

Mat erosion(Mat img, int threshold) {
	Mat erode = Mat::zeros(img.size(), CV_8UC1);

	for (int i = threshold; i < img.rows - threshold; i++) {
		for (int j = threshold; j < img.cols - threshold; j++) {
			int noblack = false;
			for (int a = -threshold; a <= threshold; a++) {
				for (int b = -threshold; b <= threshold; b++) {
					if (img.at<uchar>(i + a, j + b) == 0) {
						noblack = true;
					}
				}
			}

			if (!noblack) {
				erode.at<uchar>(i, j) = 255;
			}
		}
	}
	return erode;
}

float OTSU(Mat bw) {
	float count[256] = { 0 };
	float total = bw.rows * bw.cols;
	float optimized;

	//Get i
	for (int i = 0; i < bw.rows; i++) {
		for (int j = 0; j < bw.cols; j++) {
			count[bw.at<uchar>(i, j)]++;
		}
	}

	// Get i probability
	float prob[256] = { 0 };
	for (int i = 0; i < 256; i++) {
		prob[i] = count[i] / total;
	}

	//Accumalative
	double theta[256] = { 0 };
	theta[0] = prob[0];
	for (int i = 1; i < 256; i++) {
		theta[i] = (prob[i] + theta[i - 1]);
	}

	//Calculate M
	float mew[256] = { 0 };
	for (int i = 0; i < 256; i++) {
		mew[i] = mew[i - 1] + (i * prob[i]);
	}

	float sigma[256] = { 0 };
	for (int i = 0; i < 256; i++) {
		sigma[i] = (((mew[255] * theta[i]) - mew[i]) * ((mew[255] * theta[i]) - mew[i])) / (theta[i] * (1 - theta[i]));
	}

	int temp = 0;
	int otsu = 0;
	for (int i = 0; i < 256; i++)
	{
		if (sigma[i] > temp) {
			temp = sigma[i];
			otsu = i;
		}
	}
	return otsu + 30;
}

Mat invert4tess(Mat img) {
	Mat invert = Mat::zeros(Size(70, 70), CV_8UC1);
	resize(img, img, Size(50, 50));
	int height = img.rows;
	int width = img.cols;
	int white = 0;
	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			if (img.at<uchar>(i, j) > 100) {
				invert.at<uchar>(i + 9, j + 9) = 255;
				white++;
			}
		}
	}
	double density = (double)white / (double)(height * width);
	if (density < 0.35) {
		invert = dilation(invert, 1);
	}
	else if (density > 0.7) {
		invert = erosion(invert, 1);
	}

	//invert = dilation(invert, 1);
	//invert = erosion(invert, 1);

	return invert;
}

void loadTesseract(Mat originalimg) {
	carplatechar.clear();
	sort(carplatesloc.begin(), carplatesloc.end());
	for (int carplateloc : carplatesloc) {
		string carplate = "C:\\carplate\\" + naming + "-" + to_string(carplateloc) + ".jpg";
		carplate = "tesseract.exe " + carplate;
		const char *b = " C:\\Users\\HappyBoy\\Desktop\\result --psm 10 > nul";
		carplate += b;
		const char *command = carplate.c_str();

		system(command);
		fstream f("C:\\Users\\HappyBoy\\Desktop\\result.txt", fstream::in);
		string s;
		getline(f, s);
		carplatechar.push_back(s);
		f.close();
	}
	system("cls");
	cout << "Carplate : ";
	int x = 50;
	for (string carplate : carplatechar) {
		putText(originalimg, carplate, cvPoint(x, 80), CV_FONT_HERSHEY_DUPLEX, 1.5, cvScalar(0, 255, 0), 1, CV_AA);
		cout << carplate;
		x += 30;
	}
	imshow("Result", originalimg);
	cout << endl;
	carplatesloc.clear();
}

void seperateWord(Mat img) {
	Mat Blob;
	Blob = img.clone();
	vector<vector<Point>> contours1;
	vector<Vec4i> hierarchy1;
	findContours(img, contours1, hierarchy1, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE, Point(0, 0));
	Mat segmented = Mat::zeros(img.size(), CV_8UC3);
	if (!contours1.empty()) {
		for (int i = 0; i < contours1.size(); i++) {
			Scalar color((rand() & 255), (rand() & 255), (rand() & 255));
			drawContours(segmented, contours1, i, color, CV_FILLED, 8, hierarchy1);
		}
	}
	Rect BlobRect;

	Scalar black = CV_RGB(0, 0, 0);
	int inc = 0;

	for (int j = 0; j < contours1.size(); j++) {
		inc++;
		BlobRect = boundingRect(contours1[j]);

		Mat tmp = img(BlobRect);
		int white = 0;
		for (int i = 0; i < tmp.rows; i++) {
			for (int j = 0; j < tmp.cols; j++) {
				if (tmp.at<uchar>(i, j) > 0) {
					white++;
				}
			}
		}

		if (BlobRect.height < BlobRect.width || (int)BlobRect.x < 3 || BlobRect.width < 2) {
			drawContours(Blob, contours1, j, black, CV_FILLED, 8, hierarchy1);
		} else {
			double density = (double)white / (double)(tmp.rows * tmp.cols);
			cout << "Density : " << density << endl;
			Mat seperated = img(BlobRect);
			string carplate = "C:\\carplate\\" + naming + "-" + to_string(BlobRect.x) + ".jpg";
				
			imwrite(carplate, invert4tess(seperated));
			carplatesloc.push_back(BlobRect.x);
		}
	}
}

Mat segmentation(Mat img, Mat gray) {
	Mat Blob;
	Blob = img.clone();
	vector<vector<Point>> contours1;
	vector<Vec4i> hierarchy1;
	findContours(img, contours1, hierarchy1, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE, Point(0, 0));
	Mat segmented = Mat::zeros(img.size(), CV_8UC3);
	if (!contours1.empty()) {
		for (int i = 0; i < contours1.size(); i++) {
			Scalar color((rand() & 255), (rand() & 255), (rand() & 255));
			drawContours(segmented, contours1, i, color, CV_FILLED, 8, hierarchy1);
		}
	}
	
	Rect BlobRect;
	Mat plate;
	Scalar black = CV_RGB(0,0,0);
	int bottom = (img.size().height / 100) * 90;
	int top = img.size().height - bottom;
	int right = (img.size().width / 100) * 80;
	int left = img.size().width - right;
	cout << img.size()<<endl;

	for (int j = 0; j < contours1.size(); j++) {
		BlobRect = boundingRect(contours1[j]);
		Mat tmp = img(BlobRect);
		int white = 0;
		for (int i = 0; i < tmp.rows; i++) {
			for (int j = 0; j < tmp.cols; j++) {
				if (tmp.at<uchar>(i, j) > 0) {
					white++;
				}
			}
		}

		string showme = to_string(BlobRect.size().width) + "," + to_string(BlobRect.size().height);
		putText(segmented, showme, cvPoint(BlobRect.x, BlobRect.y),FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(200, 200, 250), 1, CV_AA);
		double ratio = (double)BlobRect.width / (double)BlobRect.height;

		if (BlobRect.height > BlobRect.width || BlobRect.width < 50 || ratio < 1.4 || BlobRect.height < 23 || BlobRect.x < 100 || BlobRect.x > right || BlobRect.y < 150 || BlobRect.y > bottom) {
			drawContours(Blob, contours1,j , black, CV_FILLED, 8, hierarchy1);
		} else {
			cout << BlobRect.x << " , " << BlobRect.y << endl;
			if (((double)white / (double)(tmp.rows * tmp.cols)) > 0.4) {
				return gray(BlobRect);
			}
		}
	}
	return segmented;
}

//License Plate Detection
void LPD(Mat img) {
	Mat grayscale = makemegray(img);
	Mat car = equalize(grayscale);
	car = blur(grayscale, 1);
	int carotsu = OTSU(car);
	if(carotsu > 140 && carotsu < 150)
		car = edgeDetection(car, 40);
	else
		car = edgeDetection(car, 80);

	car = dilation(car, 5);

	Mat carplate = segmentation(car, grayscale);
	imshow("Carplate", carplate);

	//Start carplate character splitting
	resize(carplate, carplate, Size(carplate.size().width + 50, carplate.size().height + 20));


	int otsu = OTSU(carplate) + 50;

	if ((otsu < 180 && otsu > 170) || otsu > 230)
		otsu -= 40;
	else if (otsu == 200)
		otsu += 30;

	carplate = binarize(carplate, otsu);
	seperateWord(carplate);
	loadTesseract(img);
	cout << "OTSU for car : " << carotsu << endl;
	cout << "OTSU for carplate : " << otsu << endl;

	waitKey();
}

int main()
{
	int start = 1;
	int thres = 20;
	for (int carimg = start; carimg < thres + 1; carimg++) {
		string carplate = "C://Users//jonathan.law//Desktop//Dataset//" + to_string(carimg) + ".jpg";
		naming = to_string(carimg);
		Mat img = imread(carplate);
		LPD(img);
	}

	waitKey();
	return 0;
}